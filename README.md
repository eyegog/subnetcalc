# subnetcalc - Command-line subnet calculator
Small toy subnet calculator for IPv4 subnets written in C.

## Usage
subnetcalc can accept either:
- A network address and mask e.g. `192.168.0.254 255.255.255.0`
- A network address and mask length in CIDR notation e.g. `192.168.0.254/24`

### Example
```
$ subnetcalc 192.168.0.255 255.255.255.0
CIDR notation:                192.168.0.0/24
Network address:              192.168.0.0
Broadcast address:            192.168.0.255
Network mask:                 255.255.255.0
Network bits:                 24
Host bits:                    8
Host address range:           192.168.0.1 - 192.168.0.254
Number of host addresses:     254

$ subnetcalc 192.168.0.255/24
CIDR notation:                192.168.0.0/24
Network address:              192.168.0.0
Broadcast address:            192.168.0.255
Network mask:                 255.255.255.0
Network bits:                 24
Host bits:                    8
Host address range:           192.168.0.1 - 192.168.0.254
Number of host addresses:     254
```

## Building and installing
Using make:
```
$ make
$ sudo make install
```
Or just:
```
$ cc subnetcalc.c -o subnetcalc
$ cp -v subnetcalc /usr/local/bin
```

#### Uninstalling
Using make:
```
$ sudo make uninstall
```

Or just:
```
$ sudo rm -v /usr/local/bin/subnetcalc
```
