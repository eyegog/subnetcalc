#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>
#include<string.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<errno.h>

struct subnet {
    uint32_t network;
    uint32_t broadcast;
    uint32_t mask;
    int      masklen;
    uint32_t fhost;
    uint32_t lhost;
    int      hostaddrs;
};

void usage() {
    printf("Usage: subnetcal <cidr>|(<ip_address> <netmask>)\n");
}

/* Fills out the rest of the info in a struct subnet so long
 * as the network and mask have been set
 */
void calculate_subnet(struct subnet *net)
{
    /* Get the addresses in host byte order so we can do arithmetic with them */
    uint32_t networkcopy = ntohl(net->network);
    uint32_t maskcopy = ntohl(net->mask);

    /* Get the length of the mask by bit shifting it until it becomes 0 */
    int masklen;
    for (masklen = 0; maskcopy > 0; masklen++)
        maskcopy = maskcopy << 1;

    net->masklen = masklen;
    /* Get number of valid host addresses in this subnet */
    net->hostaddrs = (1 << (32 - masklen)) - 2;
    /* Get first host address */
    net->fhost = htonl(networkcopy + 1);
    /* Get last host address */
    net->lhost = htonl(networkcopy + net->hostaddrs);
    /* Get broadcast_str address */
    net->broadcast = htonl(networkcopy + net->hostaddrs + 1);
}

void print_subnet(struct subnet *net)
{
    char network_str[INET_ADDRSTRLEN], netmask_str[INET_ADDRSTRLEN], broadcast_str[INET_ADDRSTRLEN],
        fhost_str[INET_ADDRSTRLEN], lhost_str[INET_ADDRSTRLEN];

    /* Convert all the attributes in the subnet */
    inet_ntop(AF_INET, &net->network, network_str, INET_ADDRSTRLEN);
    inet_ntop(AF_INET, &net->broadcast, broadcast_str, INET_ADDRSTRLEN);
    inet_ntop(AF_INET, &net->mask, netmask_str, INET_ADDRSTRLEN);
    inet_ntop(AF_INET, &net->fhost, fhost_str, INET_ADDRSTRLEN);
    inet_ntop(AF_INET, &net->lhost, lhost_str, INET_ADDRSTRLEN);

    printf("CIDR notation:                %s/%d\n", network_str, net->masklen);
    printf("Network address:              %s\n", network_str);
    printf("Broadcast address:            %s\n", broadcast_str);
    printf("Network mask:                 %s\n", netmask_str);
    printf("Network bits:                 %d\n", net->masklen);
    printf("Host bits:                    %d\n", (32 - net->masklen));
    printf("Host address range:           %s - %s\n", fhost_str, lhost_str);
    printf("Number of host addresses:     %d\n", net->hostaddrs);
}

int main(int argc, char *argv[])
{
    struct subnet net;

    /* If there is only 1 positional argument then the user has supplied a CIDR */
    if (argc == 2) {
        char *string = argv[1];
        char *network_str, *bit_str;
        if ((network_str = strsep(&string, "/")) == NULL) {
            fprintf(stderr, "ERROR: Invalid CIDR notation '%s'\n", string);
            exit(1);
        }
        if ((bit_str = strsep(&string, "/")) == NULL) {
            fprintf(stderr, "ERROR: Invalid CIDR notation '%s'\n", string);
            exit(1);
        }

        long hostbits = 32 - strtol(bit_str, NULL, 10);
        /* Netmask must be at least /31 */
        if (hostbits < 1) {
            fprintf(stderr, "ERROR: Invalid network mask length '%s'\n", bit_str);
            exit(1);
        }


        /* Create the netmask from the mask length */
        uint32_t netmask = ~ 0;
        for(int i = 0; i < hostbits; i++)
            netmask -= 1 << i;
        net.mask = htonl(netmask);
        if (!inet_pton(AF_INET, network_str, &net.network)) {
            fprintf(stderr, "ERROR in address: %s\n", strerror(errno));
            exit(1);
        }
    }
    /* If is 3 then they have supplied an address and netmask */
    else if (argc == 3) {
        if (!inet_pton(AF_INET, argv[1], &net.network)) {
            fprintf(stderr, "ERROR in address: %s\n", strerror(errno));
            exit(1);
        }
        if (!inet_pton(AF_INET, argv[2], &net.mask)) {
            fprintf(stderr, "ERROR in netmask: %s\n", strerror(errno));
            exit(1);
        }
    }
    else {
        usage();
        exit(1);
    }

    /* The user may have supplied a host address instead of the address of the actual
     * network so AND it with the netmask to make sure we have the network address */
    net.network = net.network & net.mask;

    calculate_subnet(&net);
    print_subnet(&net);
    return 0;
}
