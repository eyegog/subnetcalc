CC=cc
INSTALLDIR=/usr/local/bin
BIN=subnetcalc

$(BIN): $(BIN).c
	$(CC) -o $@ $<

.PHONY: clean install uninstall

install:
	cp $(BIN) $(INSTALLDIR)/$(BIN)

uninstall:
	rm -f $(INSTALLDIR)/$(BIN)
clean:
	rm -f $(BIN)
